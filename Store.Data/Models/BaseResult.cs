﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Data.Models
{
    public class BaseResult
    {
        public bool Ok { get; set; }
        public string Message { get; set; }
    }
}
