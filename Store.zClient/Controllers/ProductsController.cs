﻿using Store.zClient.Classes;
using Store.zClient.Models;
using Store.zClient.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Store.zClient.Controllers
{
    public class ProductsController : Controller
    {
        private ApiService apiService;

        public ProductsController()
        {
            apiService = new ApiService();
        }

        // GET: Products
        public ActionResult Index()
        {            
            return View();
        }

        [HttpGet, ActionName("ListProducts")]
        public async Task<PartialViewResult> GetProducts()
        {
            var products = await apiService.Get<Product>("http://localhost:55827", "/api", "/Products");
            
            return PartialView("_ListProducts", (List<Product>) products.Result);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<JsonResult> Create(string nameProduct, string DescriptionProduct)
        {
            var _product = new Product
            {
                Active = true,
                CreateDate = DateTime.Now,
                Description = DescriptionProduct,
                Name = nameProduct,
                Price = 0,
                UpdateDate = DateTime.Now
            };

            var result = new BaseResult();

            try
            {
                var response = await apiService.Post("http://localhost:55827", "/api", "/Products", _product);

                if (!response.IsSuccess)
                {
                    result.Ok = false;
                    result.Message = "Error al guardar el producto";
                }
                else
                {
                    result.Ok = true;
                    result.Message = "Producto guardado con exito";
                }               
            }
            catch (Exception ex)
            {
                result.Ok = false;
                result.Message = "Error al guardar: " + ex.Message;
            }

            return Json(result);
        }
        
        // GET: Products/Edit/5
        public async Task<JsonResult> Edit(int? id)
        {            
            var product = new Product();
            product.Id = (int)id;
            var response = await apiService.GetById<Product>("http://localhost:55827", "/api", "/Products", product);

            if(response.IsSuccess == false)
            {
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            return Json(response.Result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<JsonResult> Edit(int? id, string nameProduct, string DescriptionProduct)
        {
            var result = new BaseResult();

            if (id == null)
            {
                result.Ok = false;
                result.Message = "Error al actualizar el producto";
            }

            var product = new Product();
            product.Id = (int)id;
            var response = await apiService.GetById<Product>("http://localhost:55827", "/api", "/Products", product);            

            if (response.IsSuccess == false)
            {
                result.Ok = false;
                result.Message = "Error al actualizar el producto";
            }

            product = (Product)response.Result;
            product.Description = DescriptionProduct;
            product.Name = nameProduct;
            product.UpdateDate = DateTime.Now;

            try
            {
                response = await apiService.Put("http://localhost:55827", "/api", "/Products", product);

                if (!response.IsSuccess)
                {
                    result.Ok = false;
                    result.Message = "Error al actualizar el producto";
                }
                else
                {
                    result.Ok = true;
                    result.Message = "Producto actualizado con exito";
                }
            }
            catch (Exception ex)
            {
                result.Ok = false;
                result.Message = "Error al actualizar el producto: " + ex.Message;
            }

            return Json(result);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        public async Task<JsonResult> DeleteConfirmed(int? id)
        {
            var result = new BaseResult();
            if (id == null)
            {
                result.Ok = false;
                result.Message = "El codigo no debe ser nulo";
                return Json(result);
            }

            var product = new Product();
            product.Id = (int)id;
            var response = await apiService.GetById<Product>("http://localhost:55827", "/api", "/Products", product);

            if (response.IsSuccess == false)
            {
                result.Ok = false;
                result.Message = "Error al eliminar el producto";
            }            
            
            try
            {
                response = await apiService.Delete("http://localhost:55827", "/api", "/Products", product);

                if (!response.IsSuccess)
                {
                    result.Ok = false;
                    result.Message = "Error al eliminar el producto";
                }
                else
                {
                    result.Ok = true;
                    result.Message = "Producto eliminado con exito";
                }                
            }
            catch (Exception ex)
            {
                result.Ok = false;
                result.Message = "Error al eliminar el producto: " + ex.Message;
            }
            return Json(result);
        }

        public async Task<JsonResult> StateProduct(int? id)
        {
            var result = new BaseResult();

            if (id == null)
            {
                result.Ok = false;
                result.Message = "No se encontró el producto";
            }

            var product = new Product();
            product.Id = (int)id;
            var response = await apiService.GetById<Product>("http://localhost:55827", "/api", "/Products", product);

            if (response.IsSuccess == false)
            {
                result.Ok = false;
                result.Message = "Error al actualizar el producto";
            }

            product = (Product)response.Result;
            product.Active = product.Active == false ? true : false;

            try
            {
                response = await apiService.Put("http://localhost:55827", "/api", "/Products", product);

                if (!response.IsSuccess)
                {
                    result.Ok = false;
                    result.Message = "Error al actualizar el producto";
                }
                else
                {
                    result.Ok = true;
                    result.Message = "Producto actualizado con exito";
                }
            }
            catch (Exception ex)
            {
                result.Ok = false;
                result.Message = "Error al actualizar el producto: " + ex.Message;
            }

            return Json(result);
        }
    }
}