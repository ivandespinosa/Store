﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Store.zClient.Classes
{
    public class BaseResult
    {
        public bool Ok { get; set; }
        public string Message { get; set; }
    }
}