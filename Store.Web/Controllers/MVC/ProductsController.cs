﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Store.Data.Models;
using Store.Repository.Repository;

namespace Store.Web.Controllers.MVC
{
    public class ProductsController : Controller
    {
        private IRepository<Product> _repository;

        public ProductsController(IRepository<Product> repository)
        {
            _repository = repository;
        }

        private DataContext db = new DataContext();

        // GET: Products
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet, ActionName("ListProducts")]
        public PartialViewResult GetProducts()
        {
            var products = _repository.ReadAll().ToList();
            return PartialView("_ListProducts", products.OrderBy(p => p.Name));
        }

        // GET: Products/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            
            var product = _repository.GetById((int)id);

            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // GET: Products/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Products/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public JsonResult Create(string nameProduct, string DescriptionProduct)
        {
            var _product = new Product
            {
                Active = true,
                CreateDate = DateTime.Now,
                Description = DescriptionProduct,
                Name = nameProduct,
                Price = 0,
                UpdateDate = DateTime.Now
            };

            var result = new BaseResult();

            try
            {
                _repository.Create(_product);
                result.Ok = true;
                result.Message = "Producto guardado con exito";
            }
            catch (Exception ex)
            {
                result.Ok = false;
                result.Message = "Error al guardar: " + ex.Message;
            }

            return Json(result);
        }

        // GET: Products/Edit/5
        public JsonResult Edit(int? id)
        {
            var result = new BaseResult();
            if (id == null)
            {
                result.Ok = false;
                result.Message = "No se puede encontar el producto";
                return Json(result);
            }
            var product = _repository.GetById((int)id);
            if (product == null)
            {
                result.Ok = false;
                result.Message = "No se puede encontar el producto";
                return Json(result);
            }

            return Json(product, JsonRequestBehavior.AllowGet);
        }

        // POST: Products/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public JsonResult Edit(int? id, string nameProduct, string DescriptionProduct)
        {           
            var result = new BaseResult();

            if (id == null)
            {
                result.Ok = false;
                result.Message = "Error al actualizar el producto";
            }

            var product = _repository.GetById((int)id);

            if (product == null)
            {
                result.Ok = false;
                result.Message = "Error al actualizar el producto";
            }

            product.Description = DescriptionProduct;
            product.Name = nameProduct;
            product.UpdateDate = DateTime.Now;

            try
            {
                _repository.Update(product);
                result.Ok = true;
                result.Message = "Producto actualizado con éxito";
            }
            catch (Exception ex)
            {
                result.Ok = false;
                result.Message = "Error al actualizar el producto: " + ex.Message;
            }
                        
            return Json(result);
        }

        // GET: Products/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var product = _repository.GetById((int)id);
            
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        public JsonResult DeleteConfirmed(int? id)
        {
            var result = new BaseResult();
            if (id == null) {
                result.Ok = false;
                result.Message = "El codigo no debe ser nulo";
                return Json(result);
            }

            var product = _repository.GetById((int)id);

            if (product == null)
            {
                result.Ok = false;
                result.Message = "El producto no existe";
                return Json(result);
            }

            try
            {
                _repository.Delete(product.Id);
                result.Ok = true;
                result.Message = "Producto eliminado con éxito";
            }
            catch (Exception ex)
            {
                result.Ok = false;
                result.Message = "Error al eliminar el producto: " + ex.Message;
            }
            return Json(result);
        }

        public JsonResult StateProduct(int? id)
        {
            var result = new BaseResult();

            if(id == null)
            {
                result.Ok = false;
                result.Message = "No se encontró el producto";
            }

            var product = _repository.GetById((int)id);

            if (product == null)
            {
                result.Ok = false;
                result.Message = "No se encontró el producto";
            }

            product.Active = product.Active == false ? true : false;

            try
            {
                _repository.Update(product);
                result.Ok = true;
                result.Message = "Producto actualizado con éxito";
            }
            catch (Exception ex)
            {
                result.Ok = false;
                result.Message = "Error al actualizar el producto: " + ex.Message;
            }

            return Json(result);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
