﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Store.zClient.Models
{
    public class Product
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public int Id { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public bool Active { get; set; }

        public override int GetHashCode()
        {
            return Id;
        }
    }
}