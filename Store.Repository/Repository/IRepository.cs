﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Repository.Repository
{
    public interface IRepository<T>
    {
        void Create(T entity);

        void Delete(int id);

        void Update(T entity);

        T GetById(int id);

        IEnumerable<T> ReadAll();
    }
}
