﻿using Store.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Repository.Repository
{
    public class Repository<T> : IRepository<T> where T : BaseEntity, new()
    {
        public void Create(T entity)
        {
            using(var db=new DataContext())
            {
                db.Entry(entity).State = EntityState.Added;
                db.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            using (var db = new DataContext())
            {
                var entity = new T() { Id = id };
                db.Entry(entity).State = EntityState.Deleted;
                db.SaveChanges();
            }
        }

        public T GetById(int id)
        {
            using (var db = new DataContext())
            {
                return db.Set<T>().FirstOrDefault(x => x.Id == id);
            }
        }

        public IEnumerable<T> ReadAll()
        {
            using (var db = new DataContext())
            {
                var result = db.Set<T>().ToList();
                return result;
             
            }
        }

        public void Update(T entity)
        {
            using (var db = new DataContext())
            {
                db.Entry(entity).State = EntityState.Modified;
                db.SaveChanges();
            }
        }
    }
}
